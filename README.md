1. To run test locally use 'mvn verify -DAPI_KEY=put_accsess_key_here'
2. New feature files should be located in resources/features folder
3. Implementation (step definition) should be located in stepdefinition folder and extend CommonStepsDefinition class
4. New API calls should be located in api folder and extend BaseApiCalls
5. Serenity report is artefact of gitlab pipeline run, just download it, unzip and click index.html to see report
6. Documentation for API under test located by this link https://aviationstack.com/documentation, for now there's some test coverage for endpoint /flights
Feature: Test flights API

  Scenario Outline: Test real-time flights API (positive)
    Given user would like to send request with limit '<limit>' and offset '<offset>'
    When user make call with given query parameters
    Then request is success
    And user can see limit '<limit>' and offset '<offset>' in response
    And user can see '<limit>' flights in response

    Examples:
      | limit | offset |
      | 1     | 0      |
      | 100   | 10     |

  Scenario: Test real-time flights API (negative)
    Given user would like to send request with limit '101' and offset '10'
    When user make call with given query parameters
    Then request is failed
    And user can see error code 'function_access_restricted'
    And user can see error message 'Your current subscription plan does not support this API function.'
package dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Flights {

    private Pagination pagination;
    private List<Data> data;

    @lombok.Data
    public static class Pagination {

        private String limit;
        private String offset;
        private String count;
        private String total;

    }

    @lombok.Data
    public static class Data {
        public String flight_date;
        public String flight_status;
        public Departure departure;
        public Arrival arrival;
        public Airline airline;
        public Flight flight;
        public Aircraft aircraft;
        public Live live;

        public class Departure {
            public String airport;
            public String timezone;
            public String iata;
            public String icao;
            public String terminal;
            public String gate;
            public int delay;
            public Date scheduled;
            public Date estimated;
            public Date actual;
            public Date estimated_runway;
            public Date actual_runway;
        }

        public class Arrival {
            public String airport;
            public String timezone;
            public String iata;
            public String icao;
            public String terminal;
            public String gate;
            public String baggage;
            public int delay;
            public Date scheduled;
            public Date estimated;
            public Object actual;
            public Object estimated_runway;
            public Object actual_runway;
        }

        public class Airline {
            public String name;
            public String iata;
            public String icao;
        }

        public class Flight {
            public String number;
            public String iata;
            public String icao;
            public Object codeshared;
        }

        public class Aircraft {
            public String registration;
            public String iata;
            public String icao;
            public String icao24;
        }

        public class Live {
            public Date updated;
            public double latitude;
            public double longitude;
            public double altitude;
            public double direction;
            public double speed_horizontal;
            public double speed_vertical;
            public boolean is_ground;
        }
    }
}

package dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlightsApiError {

    private Error error;

    @Data
    public static class Error {

        private String code;
        private String message;

    }
}

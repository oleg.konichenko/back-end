package api;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.apache.commons.collections4.MapUtils.emptyIfNull;

public class BaseApiCalls {

    protected Endpoints endpoints = new Endpoints();

    private static final String HOST = "http://api.aviationstack.com";
    private static final String API_VERSION = "/v1";

    protected Response get(String uri, Map<String, String> queryParameters, String apiKey) {
        RequestSpecification request = given().redirects().follow(false);
        request.queryParam("access_key", apiKey);
        emptyIfNull(queryParameters).forEach(request::queryParam);
        return request
                .relaxedHTTPSValidation()
                .when()
                .log().method()
                .log().uri()
                .get(HOST + API_VERSION + uri);
    }
}

package api;

import io.restassured.response.Response;

import java.util.Map;

public class FlightsApiCalls extends BaseApiCalls {

    public Response getFlights(Map<String, String> queryParameters, String apiKey) {
        return  get(endpoints.getFlights(), queryParameters, apiKey);
    }
}

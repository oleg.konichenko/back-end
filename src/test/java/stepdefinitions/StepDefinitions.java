package stepdefinitions;

import dto.Flights;
import dto.FlightsApiError;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.assertj.core.api.SoftAssertions;

import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class StepDefinitions extends CommonStepsDefinition {

    @Given("user would like to send request with limit {string} and offset {string}")
    public void userSetLimitAndOffset(String limit, String offset) {
        Map<String, String> queryParameters = Map.of("limit", limit, "offset", offset);
        testContext.set(Map.of("queryParams", queryParameters));
    }

    @When("user make call with given query parameters")
    public void userMakeCall() {
        Map<String, String> queryParameters = (Map<String, String>) testContext.get().get("queryParams");
        Response response = flightsCalls.getFlights(queryParameters, System.getProperty("API_KEY"));
        testContext.set(Map.of("response", response));
    }

    @Then("request is success")
    public void requestIsSuccess() {
        Response response = (Response) testContext.get().get("response");
        assertThat(response.getStatusCode())
                .as("Expected success request")
                .isBetween(200,204);
    }

    @Then("request is failed")
    public void requestIsFailed() {
        Response response = (Response) testContext.get().get("response");
        assertThat(response.getStatusCode())
                .as("Expected success request")
                .isNotIn(200, 201, 202, 203, 204);
    }

    @Then("user can see limit {string} and offset {string} in response")
    public void userCanSeeLimitAndOffset(String limit, String offset) {
        Response response = (Response) testContext.get().get("response");
        Flights flights = response.as(Flights.class);
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(flights.getPagination().getLimit())
                    .as("Limits in request and response are not the same")
                    .isEqualTo(limit);
            softly.assertThat(flights.getPagination().getOffset())
                    .as("Offsets in request and response are not the same")
                    .isEqualTo(offset);
        });
    }

    @Then("user can see {string} flights in response")
    public void userCanSeeFlights(String count) {
        Response response = (Response) testContext.get().get("response");
        Flights flights = response.as(Flights.class);
        assertThat(flights.getData().size())
                .as("Flights count is not equals limit")
                .isEqualTo(Integer.parseInt(count));
    }

    @Then("user can see error code {string}")
    public void userCanSeeErrorCode(String expectedErrorCode) {
        Response response = (Response) testContext.get().get("response");
        FlightsApiError error = response.as(FlightsApiError.class);
        assertThat(error.getError().getCode())
                .as("Wrong API error code")
                .isEqualTo(expectedErrorCode);
    }

    @Then("user can see error message {string}")
    public void userCanSeeErrorMessage(String expectedErrorMessage) {
        Response response = (Response) testContext.get().get("response");
        FlightsApiError error = response.as(FlightsApiError.class);
        assertThat(error.getError().getMessage())
                .as("Wrong error message")
                .isEqualTo(expectedErrorMessage);
    }
}

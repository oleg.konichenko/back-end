package core;

import java.util.Map;

public class TestContext {

    private final ThreadLocal<Map<String,Object>> testContext =
            ThreadLocal.withInitial(Map::of);

    public Map<String, Object> get() {
        return testContext.get();
    }

    public void set(Map<String, Object> object) {
        testContext.set(object);
    }

    public void reset() {
        testContext.remove();
    }
}
